## SpaceY Challenge

### API Purpose :

Compare two tables (**source** against **destination**) and outputs
the differences between them if any.

The aspects which were chosen to compare the tables :

- Object name (schema name and table name)
- Structure (columns)
- Row count
- Data

The designed API can be found in the repository section
under **src/main/resources/openapi.yaml**.

The project is package as a docker container using jib.
The container can be pulled from dockerhub under : **oscarkowu/spacey:latest**

There is a docker-compose file under **src/test/resources** with a sample database for
testing the app.

The structure of the database can be found under the **src/test/resources/mysql/init.sql**

When up, you should be able to access the API via **http://localhost:8080/tables/compare**
with a tool like POSTMAN and specify the required body for the comparison.