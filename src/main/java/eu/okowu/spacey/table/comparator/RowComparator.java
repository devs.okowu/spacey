package eu.okowu.spacey.table.comparator;

import eu.okowu.spacey.model.RowComparison;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RowComparator {

  public static RowComparison compare(final int sourceRows, final int destinationRows) {

    return sourceRows == destinationRows
        ? null
        : new RowComparison().sourceRows(sourceRows).destinationRows(destinationRows);
  }
}
