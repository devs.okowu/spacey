package eu.okowu.spacey.table.comparator;

import eu.okowu.spacey.model.ObjectNameComparison;
import eu.okowu.spacey.model.TableInformation;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ObjectNameComparator {

  public static ObjectNameComparison compare(
      final TableInformation source, final TableInformation destination) {

    if (compareName(source.getSchemaName(), destination.getSchemaName())
        && compareName(source.getTableName(), destination.getTableName())) {
      return null;
    }

    return new ObjectNameComparison()
        .sourceObjectName(computeObjectName(source))
        .destinationObjectName(computeObjectName(destination));
  }

  private static boolean compareName(final String source, final String destination) {
    return source.equalsIgnoreCase(destination);
  }

  private static String computeObjectName(final TableInformation tableInformation) {
    return tableInformation.getSchemaName().concat(".").concat(tableInformation.getTableName());
  }
}
