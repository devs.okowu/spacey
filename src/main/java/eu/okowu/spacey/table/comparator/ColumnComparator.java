package eu.okowu.spacey.table.comparator;

import eu.okowu.spacey.model.ColumnComparison;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ColumnComparator {

  public static ColumnComparison compare(
      final List<String> sourceColumns, final List<String> destinationColumns) {

    final Set<String> sourceColumnsSet = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
    final Set<String> destinationColumnsSet = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

    sourceColumnsSet.addAll(sourceColumns);
    destinationColumnsSet.addAll(destinationColumns);

    final List<String> identical = compareColumns(sourceColumnsSet, destinationColumnsSet, true);

    return new ColumnComparison()
        .sourceCount(sourceColumnsSet.size())
        .destinationCount(destinationColumnsSet.size())
        .identical(identical)
        .onlyInSource(compareColumns(sourceColumnsSet, destinationColumnsSet, false))
        .onlyInDestination(compareColumns(destinationColumnsSet, sourceColumnsSet, false));
  }

  private static List<String> compareColumns(
      final Set<String> source, final Set<String> target, boolean equals) {
    return source.stream()
        .filter(column -> equals == target.contains(column))
        .collect(Collectors.toUnmodifiableList());
  }
}
