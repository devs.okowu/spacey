package eu.okowu.spacey.table;

import eu.okowu.spacey.model.TableInformation;
import eu.okowu.spacey.table.query.MysqlComparisonQuery;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class TableRepositoryImpl implements TableRepository {

  private final EntityManager entityManager;
  private final MysqlComparisonQuery mysqlComparisonQuery;

  @Override
  public boolean exists(final TableInformation tableInformation) {
    final Query query = entityManager.createNativeQuery(mysqlComparisonQuery.tableExists());
    query.setParameter("tableSchema", tableInformation.getSchemaName());
    query.setParameter("tableName", tableInformation.getTableName());
    final List<Number> result = typedList(query.getResultList(), Number.class);
    return result.get(0).intValue() == 1;
  }

  @Override
  public int getRowCount(final TableInformation tableInformation) {
    final Query query =
        entityManager.createNativeQuery(mysqlComparisonQuery.getRowCount(tableInformation));
    final List<Number> result = typedList(query.getResultList(), Number.class);
    return result.isEmpty() ? 0 : result.get(0).intValue();
  }

  @Override
  public List<String> getColumns(final TableInformation tableInformation) {
    final Query query = entityManager.createNativeQuery(mysqlComparisonQuery.getTableColumns());
    query.setParameter("tableSchema", tableInformation.getSchemaName());
    query.setParameter("tableName", tableInformation.getTableName());
    return typedList(query.getResultList(), String.class);
  }

  @Override
  public int getDataDifference(
      final TableInformation target, final TableInformation against, final List<String> columns) {
    final Query query =
        entityManager.createNativeQuery(
            mysqlComparisonQuery.getDataDifference(target, against, columns));
    final List<Number> result = typedList(query.getResultList(), Number.class);
    return result.isEmpty() ? 0 : result.get(0).intValue();
  }

  private <T> List<T> typedList(final List<?> source, final Class<T> clazz) {
    return source.stream().map(clazz::cast).collect(Collectors.toUnmodifiableList());
  }
}
