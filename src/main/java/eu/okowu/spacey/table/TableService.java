package eu.okowu.spacey.table;

import eu.okowu.spacey.MessageSourceHelper;
import eu.okowu.spacey.exception.TableNotFoundException;
import eu.okowu.spacey.model.*;
import eu.okowu.spacey.table.comparator.ColumnComparator;
import eu.okowu.spacey.table.comparator.ObjectNameComparator;
import eu.okowu.spacey.table.comparator.RowComparator;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TableService {

  private final TableRepository tableRepository;

  public TableComparisonView compareTables(
      final TableInformation source, final TableInformation destination) {

    assertTableExists(source);
    assertTableExists(destination);

    final ObjectNameComparison objectNameComparison = compareObjectName(source, destination);
    final RowComparison rowComparison = compareRow(source, destination);
    final ColumnComparison columnComparison = compareColumn(source, destination);
    final DataComparison dataComparison =
        compareData(source, destination, columnComparison.getIdentical());

    return new TableComparisonView()
        .objectName(objectNameComparison)
        .rows(rowComparison)
        .columns(columnComparison)
        .data(dataComparison);
  }

  private ObjectNameComparison compareObjectName(
      final TableInformation source, final TableInformation destination) {
    return ObjectNameComparator.compare(source, destination);
  }

  private RowComparison compareRow(
      final TableInformation source, final TableInformation destination) {
    final int sourceRows = tableRepository.getRowCount(source);
    final int destinationRows = tableRepository.getRowCount(destination);
    return RowComparator.compare(sourceRows, destinationRows);
  }

  private ColumnComparison compareColumn(
      final TableInformation source, final TableInformation destination) {
    final List<String> sourceColumns = tableRepository.getColumns(source);
    final List<String> destinationColumns = tableRepository.getColumns(destination);
    return ColumnComparator.compare(sourceColumns, destinationColumns);
  }

  private DataComparison compareData(
      final TableInformation source,
      final TableInformation destination,
      final List<String> identicalColumns) {
    if (identicalColumns.isEmpty()) {
      return null;
    }

    final int missingData =
        tableRepository.getDataDifference(source, destination, identicalColumns);
    final int additionalData =
        tableRepository.getDataDifference(destination, source, identicalColumns);
    return new DataComparison().missing(missingData).additional(additionalData);
  }

  private void assertTableExists(final TableInformation tableInformation) {
    if (!tableRepository.exists(tableInformation)) {
      throw new TableNotFoundException(
          MessageSourceHelper.getMessage(
              "msg.exception.table.not.found",
              tableInformation.getTableName(),
              tableInformation.getSchemaName()));
    }
  }
}
