package eu.okowu.spacey.table;

import eu.okowu.spacey.MessageSourceHelper;
import eu.okowu.spacey.api.TablesApiDelegate;
import eu.okowu.spacey.model.TableComparisonPayload;
import eu.okowu.spacey.model.TableComparisonView;
import eu.okowu.spacey.model.TableInformation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Slf4j
@Controller
@RequiredArgsConstructor
public class TableApi implements TablesApiDelegate {

  private final TableService tableService;

  @Override
  public ResponseEntity<TableComparisonView> compareTables(final TableComparisonPayload payload) {
    final TableInformation source = payload.getSource();
    final TableInformation destination = payload.getDestination();
    log.info(
        MessageSourceHelper.getMessage(
            "msg.info.table.comparison",
            source.getSchemaName().concat(".").concat(source.getTableName()),
            destination.getSchemaName().concat(".").concat(destination.getTableName())));
    return ResponseEntity.ok(tableService.compareTables(source, destination));
  }
}
