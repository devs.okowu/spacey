package eu.okowu.spacey.table;

import eu.okowu.spacey.model.TableInformation;
import java.util.List;

public interface TableRepository {
  boolean exists(final TableInformation tableInformation);

  int getRowCount(final TableInformation tableInformation);

  List<String> getColumns(final TableInformation tableIdentifier);

  int getDataDifference(
      final TableInformation source,
      final TableInformation destination,
      final List<String> columns);
}
