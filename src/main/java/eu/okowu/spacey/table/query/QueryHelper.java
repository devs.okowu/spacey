package eu.okowu.spacey.table.query;

import java.util.List;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class QueryHelper {

  public static String compareSrcColumnsAgainstDestColumn(
      final List<String> columns, final String source, final String destination) {
    return columns.stream()
        .map(column -> source + "." + column + " = " + destination + "." + column)
        .collect(Collectors.joining(" AND "));
  }

  public static String computeWhereTargetColumnsNull(
      final List<String> columns, final String target) {
    return columns.stream()
        .map(column -> target + "." + column + " IS NULL")
        .collect(Collectors.joining(" AND "));
  }
}
