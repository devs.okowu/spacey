package eu.okowu.spacey.table.query;

import eu.okowu.spacey.model.TableInformation;
import java.util.List;

public interface ComparisonQuery {

  String tableExists();

  String getTableColumns();

  String getRowCount(final TableInformation tableInformation);

  String getDataDifference(
      final TableInformation source,
      final TableInformation destination,
      final List<String> columns);
}
