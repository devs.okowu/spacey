package eu.okowu.spacey.table.query;

import eu.okowu.spacey.model.TableInformation;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class MysqlComparisonQuery implements ComparisonQuery {

  @Override
  public String tableExists() {
    return "SELECT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = :tableSchema AND TABLE_NAME = :tableName LIMIT 1)";
  }

  @Override
  public String getTableColumns() {
    return "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = :tableSchema AND TABLE_NAME = :tableName";
  }

  @Override
  public String getRowCount(final TableInformation tableInformation) {
    return String.format(
        "SELECT COUNT(*) FROM %s.%s",
        tableInformation.getSchemaName(), tableInformation.getTableName());
  }

  @Override
  public String getDataDifference(
      final TableInformation source,
      final TableInformation destination,
      final List<String> columns) {
    final String select = "SELECT COUNT(*)";
    final String from =
        String.format(
            " FROM %s.%s src LEFT JOIN %s.%s dest",
            source.getSchemaName(),
            source.getTableName(),
            destination.getSchemaName(),
            destination.getTableName());
    final String on =
        " ON " + QueryHelper.compareSrcColumnsAgainstDestColumn(columns, "src", "dest");
    final String where = " WHERE " + QueryHelper.computeWhereTargetColumnsNull(columns, "dest");
    return select + from + on + where;
  }
}
