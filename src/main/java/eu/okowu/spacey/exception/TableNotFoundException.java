package eu.okowu.spacey.exception;

public class TableNotFoundException extends RuntimeException {

  public TableNotFoundException(final String message) {
    super(message);
  }
}
