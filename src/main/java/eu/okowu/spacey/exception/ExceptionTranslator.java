package eu.okowu.spacey.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.ProblemHandling;

@ControllerAdvice
public class ExceptionTranslator implements ProblemHandling {

  @ExceptionHandler
  public ResponseEntity<Problem> handleTableNotFoundException(
      final TableNotFoundException ex, final NativeWebRequest request) {
    return create(Status.NOT_FOUND, ex, request);
  }

  @ExceptionHandler
  public ResponseEntity<Problem> handle(final RuntimeException ex, final NativeWebRequest request) {
    return create(Status.INTERNAL_SERVER_ERROR, ex, request);
  }
}
