package eu.okowu.spacey;

import java.nio.charset.StandardCharsets;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MessageSourceHelper {

  private static final ReloadableResourceBundleMessageSource messageSource;

  static {
    messageSource = new ReloadableResourceBundleMessageSource();
    messageSource.setBasename("classpath:message");
    messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
  }

  public static String getMessage(final String key, final Object... params) {
    return messageSource.getMessage(key, params, LocaleContextHolder.getLocale());
  }

  public static String getMessage(final String key) {
    return messageSource.getMessage(key, new Object[] {}, LocaleContextHolder.getLocale());
  }
}
