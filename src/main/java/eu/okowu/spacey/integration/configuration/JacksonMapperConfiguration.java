package eu.okowu.spacey.integration.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

@Configuration
public class JacksonMapperConfiguration {

  @Bean
  public MappingJackson2HttpMessageConverter jacksonConverter(final ObjectMapper objectMapper) {
    final MappingJackson2HttpMessageConverter jacksonConverter =
        new MappingJackson2HttpMessageConverter();
    jacksonConverter.setObjectMapper(objectMapper);
    return jacksonConverter;
  }
}
