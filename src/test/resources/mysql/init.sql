
CREATE TABLE IF NOT EXISTS spacey.client (
   Id bigint auto_increment NOT NULL,
   Lastname varchar(100) NULL,
   Firstname varchar(100) NULL,
   CONSTRAINT `CLIENT_PRIMARY` PRIMARY KEY (Id)
);

CREATE TABLE IF NOT EXISTS spacey.user (
     Id bigint auto_increment NOT NULL,
     Lastname varchar(100) NULL,
     Firstname varchar(100) NULL,
     CONSTRAINT `CUSTOMER_PRIMARY` PRIMARY KEY (Id)
);

CREATE TABLE IF NOT EXISTS spacey.customer (
     Id bigint auto_increment NOT NULL,
     Lastname varchar(100) NULL,
     Firstname varchar(100) NULL,
     Email varchar(100) NULL,
     CONSTRAINT `CUSTOMER_PRIMARY` PRIMARY KEY (Id)
);

INSERT INTO spacey.client (Firstname, Lastname) VALUES('Jean', 'Michel');
INSERT INTO spacey.client (Firstname, Lastname) VALUES('Francois', 'Millerand');
INSERT INTO spacey.client (Firstname, Lastname) VALUES('Blaise', 'Millet');
INSERT INTO spacey.client (Firstname, Lastname) VALUES('Victorine', 'Deschamps');
INSERT INTO spacey.client (Firstname, Lastname) VALUES('Wendy', 'Jules');

INSERT INTO spacey.customer (Firstname, Lastname) VALUES('Jeannot', 'Djondoh');
INSERT INTO spacey.customer (Firstname, Lastname) VALUES('Francois', 'Millerand');
INSERT INTO spacey.customer (Firstname, Lastname) VALUES('Blaise', 'Millet');
INSERT INTO spacey.customer (Firstname, Lastname) VALUES('Anne-laure', 'Coustot');
INSERT INTO spacey.customer (Firstname, Lastname) VALUES('Loic', 'Sanchez');
INSERT INTO spacey.customer (Firstname, Lastname) VALUES('Thierry', 'Marceau');

INSERT INTO spacey.user (Firstname, Lastname) VALUES('Jean', 'Michel');
INSERT INTO spacey.user (Firstname, Lastname) VALUES('Francois', 'Millerand');
INSERT INTO spacey.user (Firstname, Lastname) VALUES('Blaise', 'Millet');
INSERT INTO spacey.user (Firstname, Lastname) VALUES('Victorine', 'Deschamps');
INSERT INTO spacey.user (Firstname, Lastname) VALUES('Wendy', 'Jules');