CREATE SCHEMA IF NOT EXISTS test;

CREATE TABLE IF NOT EXISTS test.client (
    Id bigint auto_increment NOT NULL,
    Lastname varchar(100) NULL,
    Firstname varchar(100) NULL,
    CONSTRAINT `CLIENT_PRIMARY` PRIMARY KEY (Id)
);

CREATE TABLE IF NOT EXISTS test.user (
     Id bigint auto_increment NOT NULL,
     Lastname varchar(100) NULL,
     Firstname varchar(100) NULL,
     CONSTRAINT `CUSTOMER_PRIMARY` PRIMARY KEY (Id)
);

CREATE TABLE IF NOT EXISTS test.customer (
     Id bigint auto_increment NOT NULL,
     Lastname varchar(100) NULL,
     Firstname varchar(100) NULL,
     Email varchar(100) NULL,
     CONSTRAINT `CUSTOMER_PRIMARY` PRIMARY KEY (Id)
);

INSERT INTO test.client (Firstname, Lastname) VALUES('Jean', 'Michel');
INSERT INTO test.client (Firstname, Lastname) VALUES('Francois', 'Millerand');
INSERT INTO test.client (Firstname, Lastname) VALUES('Blaise', 'Millet');
INSERT INTO test.client (Firstname, Lastname) VALUES('Victorine', 'Deschamps');
INSERT INTO test.client (Firstname, Lastname) VALUES('Wendy', 'Jules');

INSERT INTO test.customer (Firstname, Lastname) VALUES('Jeannot', 'Djondoh');
INSERT INTO test.customer (Firstname, Lastname) VALUES('Francois', 'Millerand');
INSERT INTO test.customer (Firstname, Lastname) VALUES('Blaise', 'Millet');
INSERT INTO test.customer (Firstname, Lastname) VALUES('Anne-laure', 'Coustot');
INSERT INTO test.customer (Firstname, Lastname) VALUES('Loic', 'Sanchez');
INSERT INTO test.customer (Firstname, Lastname) VALUES('Thierry', 'Marceau');

INSERT INTO test.user (Firstname, Lastname) VALUES('Jean', 'Michel');
INSERT INTO test.user (Firstname, Lastname) VALUES('Francois', 'Millerand');
INSERT INTO test.user (Firstname, Lastname) VALUES('Blaise', 'Millet');
INSERT INTO test.user (Firstname, Lastname) VALUES('Victorine', 'Deschamps');
INSERT INTO test.user (Firstname, Lastname) VALUES('Wendy', 'Jules');