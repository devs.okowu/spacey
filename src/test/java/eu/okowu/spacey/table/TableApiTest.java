package eu.okowu.spacey.table;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eu.okowu.spacey.exception.TableNotFoundException;
import eu.okowu.spacey.model.*;
import java.util.List;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class TableApiTest {

  @Autowired private MockMvc mockMvc;
  @MockBean private TableService tableService;

  @Test
  public void compareTables_with_unknown_table_return_problem_with_not_found_status()
      throws Exception {

    final String schema = "test";
    final String sourceTable = "unknown";
    final String destinationTable = "customer";

    final TableInformation source =
        new TableInformation().schemaName(schema).tableName(sourceTable);
    final TableInformation destination =
        new TableInformation().schemaName(schema).tableName(destinationTable);
    final String payload =
        "{ \"source\": { \"schemaName\": \""
            + schema
            + "\", \"tableName\": \""
            + sourceTable
            + "\" }, \"destination\": { \"schemaName\": \""
            + schema
            + "\", \"tableName\": \""
            + destinationTable
            + "\" } }";

    given(tableService.compareTables(source, destination))
        .willThrow(new TableNotFoundException("Unknown table test.unknown"));

    mockMvc
        .perform(post("/tables/compare").contentType(MediaType.APPLICATION_JSON).content(payload))
        .andExpect(status().isNotFound())
        .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
        .andExpect(jsonPath("$.title").value("Not Found"))
        .andExpect(jsonPath("$.status").value("404"))
        .andExpect(jsonPath("$.detail").value("Unknown table test.unknown"));

    then(tableService).should(only()).compareTables(source, destination);
  }

  @Test
  public void compareTables_with_valid_tables_return_ok_status() throws Exception {
    final String schema = "test";
    final String sourceTable = "client";
    final String destinationTable = "customer";

    final TableInformation source =
        new TableInformation().schemaName(schema).tableName(sourceTable);
    final TableInformation destination =
        new TableInformation().schemaName(schema).tableName(destinationTable);

    final String payload =
        "{ \"source\": { \"schemaName\": \""
            + schema
            + "\", \"tableName\": \""
            + sourceTable
            + "\" }, \"destination\": { \"schemaName\": \""
            + schema
            + "\", \"tableName\": \""
            + destinationTable
            + "\" } }";

    final ObjectNameComparison objectNameComparison =
        new ObjectNameComparison()
            .sourceObjectName("test.client")
            .destinationObjectName("test.customer");

    final RowComparison rowComparison = new RowComparison().sourceRows(750).destinationRows(465);

    final ColumnComparison columnComparison =
        new ColumnComparison()
            .sourceCount(5)
            .destinationCount(7)
            .identical(List.of("Id", "Firstname", "Lastname"))
            .onlyInSource(List.of("Birthdate", "Gender"))
            .onlyInDestination(List.of("Email", "Street", "Zipcode", "City"));

    final DataComparison dataComparison = new DataComparison().missing(205).additional(91);

    final TableComparisonView tableComparisonView =
        new TableComparisonView()
            .objectName(objectNameComparison)
            .rows(rowComparison)
            .columns(columnComparison)
            .data(dataComparison);

    given(tableService.compareTables(source, destination)).willReturn(tableComparisonView);

    mockMvc
        .perform(post("/tables/compare").contentType(MediaType.APPLICATION_JSON).content(payload))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.objectName.sourceObjectName").value("test.client"))
        .andExpect(jsonPath("$.objectName.destinationObjectName").value("test.customer"))
        .andExpect(jsonPath("$.rows.sourceRows").value(750))
        .andExpect(jsonPath("$.rows.destinationRows").value(465))
        .andExpect(jsonPath("$.columns.sourceCount").value(5))
        .andExpect(jsonPath("$.columns.destinationCount").value(7))
        .andExpect(jsonPath("$.columns.identical", Matchers.hasSize(3)))
        .andExpect(jsonPath("$.columns.onlyInSource", Matchers.hasSize(2)))
        .andExpect(jsonPath("$.columns.onlyInDestination", Matchers.hasSize(4)))
        .andExpect(jsonPath("$.data.missing").value(205))
        .andExpect(jsonPath("$.data.additional").value(91));

    then(tableService).should(only()).compareTables(source, destination);
  }
}
