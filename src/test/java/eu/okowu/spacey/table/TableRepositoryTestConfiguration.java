package eu.okowu.spacey.table;

import eu.okowu.spacey.table.query.MysqlComparisonQuery;
import javax.persistence.EntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class TableRepositoryTestConfiguration {

  @Bean
  public MysqlComparisonQuery mysqlComparisonQuery() {
    return new MysqlComparisonQuery();
  }

  @Bean
  public TableRepositoryImpl tableRepository(
      final EntityManager entityManager, final MysqlComparisonQuery mysqlComparisonQuery) {
    return new TableRepositoryImpl(entityManager, mysqlComparisonQuery);
  }
}
