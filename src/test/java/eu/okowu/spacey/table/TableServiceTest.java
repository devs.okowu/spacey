package eu.okowu.spacey.table;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;

import eu.okowu.spacey.exception.TableNotFoundException;
import eu.okowu.spacey.model.*;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TableServiceTest {

  @InjectMocks private TableService tableService;

  @Mock private TableRepository tableRepository;

  @Test
  public void compareTables_throw_exception_when_table_does_not_exists() {

    final TableInformation unknownTable =
        new TableInformation().schemaName("unknown").tableName("table");
    final TableInformation goodTable = new TableInformation().schemaName("good").tableName("table");

    given(tableRepository.exists(unknownTable)).willReturn(false);

    assertThatThrownBy(() -> tableService.compareTables(unknownTable, goodTable))
        .isInstanceOf(TableNotFoundException.class)
        .hasMessage("The given table table in database unknown does not exists");

    then(tableRepository).should(only()).exists(unknownTable);
  }

  @Test
  public void compareTables_case_with_different_tables() {
    final TableInformation source = new TableInformation().schemaName("bank").tableName("client");
    final TableInformation destination =
        new TableInformation().schemaName("bank").tableName("customer");

    final int sourceRowCount = 158;
    final int destinationRowCount = 375;

    final List<String> sourceColumns = List.of("Id", "Firstname", "Lastname");
    final List<String> destinationColumns = List.of("Id", "Firstname", "Lastname", "Email");

    final List<String> identicalColumns = List.of("Firstname", "Id", "Lastname");

    final int missingData = 57;
    final int additionalData = 108;

    given(tableRepository.exists(source)).willReturn(true);
    given(tableRepository.exists(destination)).willReturn(true);

    given(tableRepository.getRowCount(source)).willReturn(sourceRowCount);
    given(tableRepository.getRowCount(destination)).willReturn(destinationRowCount);

    given(tableRepository.getColumns(source)).willReturn(sourceColumns);
    given(tableRepository.getColumns(destination)).willReturn(destinationColumns);

    given(tableRepository.getDataDifference(source, destination, identicalColumns))
        .willReturn(missingData);
    given(tableRepository.getDataDifference(destination, source, identicalColumns))
        .willReturn(additionalData);

    final TableComparisonView tableComparisonView = tableService.compareTables(source, destination);

    assertThat(tableComparisonView.getObjectName())
        .extracting(
            ObjectNameComparison::getSourceObjectName,
            ObjectNameComparison::getDestinationObjectName)
        .containsExactly("bank.client", "bank.customer");
    assertThat(tableComparisonView.getRows())
        .extracting(RowComparison::getSourceRows, RowComparison::getDestinationRows)
        .containsExactly(sourceRowCount, destinationRowCount);
    assertThat(tableComparisonView.getColumns())
        .extracting(
            ColumnComparison::getSourceCount,
            ColumnComparison::getDestinationCount,
            ColumnComparison::getIdentical,
            ColumnComparison::getOnlyInSource,
            ColumnComparison::getOnlyInDestination)
        .containsExactly(
            3, 4, identicalColumns, Collections.emptyList(), Collections.singletonList("Email"));
    assertThat(tableComparisonView.getData())
        .extracting(DataComparison::getMissing, DataComparison::getAdditional)
        .containsExactly(missingData, additionalData);

    then(tableRepository).should().exists(source);
    then(tableRepository).should().exists(destination);
    then(tableRepository).should().getRowCount(source);
    then(tableRepository).should().getRowCount(destination);
    then(tableRepository).should().getColumns(source);
    then(tableRepository).should().getColumns(destination);
    then(tableRepository).should().getDataDifference(source, destination, identicalColumns);
    then(tableRepository).should().getDataDifference(destination, source, identicalColumns);
    then(tableRepository).shouldHaveNoMoreInteractions();
  }
}
