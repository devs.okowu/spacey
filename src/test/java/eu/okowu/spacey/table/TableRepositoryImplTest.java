package eu.okowu.spacey.table;

import static org.assertj.core.api.Assertions.assertThat;

import eu.okowu.spacey.model.TableInformation;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(classes = {TableRepositoryTestConfiguration.class})
@ActiveProfiles("test")
public class TableRepositoryImplTest {

  @Autowired private TableRepository tableRepository;

  @Test
  public void case_when_table_exists_return_true() {
    final TableInformation tableInformation =
        new TableInformation().schemaName("test").tableName("client");
    assertThat(tableRepository.exists(tableInformation)).isTrue();
  }

  @Test
  public void case_when_table_unknown_return_false() {
    final TableInformation tableInformation =
        new TableInformation().schemaName("test").tableName("cars");
    assertThat(tableRepository.exists(tableInformation)).isFalse();
  }

  @Test
  public void getRowCount_return_exact_number() {
    final TableInformation tableInformation =
        new TableInformation().schemaName("test").tableName("client");
    assertThat(tableRepository.getRowCount(tableInformation)).isEqualTo(5);
  }

  @Test
  public void getColumns_return_exact_column_names() {
    final TableInformation tableInformation =
        new TableInformation().schemaName("test").tableName("customer");
    assertThat(tableRepository.getColumns(tableInformation))
        .containsExactlyInAnyOrder("Id", "Firstname", "Lastname", "Email");
  }

  @Test
  public void getDataDifference_case_for_missing_data_return_exact_number() {
    final TableInformation source = new TableInformation().schemaName("test").tableName("client");
    final TableInformation destination =
        new TableInformation().schemaName("test").tableName("customer");
    final List<String> columns = List.of("Id", "Firstname", "Lastname");
    assertThat(tableRepository.getDataDifference(source, destination, columns)).isEqualTo(3);
  }

  @Test
  public void getDataDifference_case_for_additional_data_return_exact_number() {
    final TableInformation source = new TableInformation().schemaName("test").tableName("client");
    final TableInformation destination =
        new TableInformation().schemaName("test").tableName("customer");
    final List<String> columns = List.of("Id", "Firstname", "Lastname");
    assertThat(tableRepository.getDataDifference(destination, source, columns)).isEqualTo(4);
  }

  @Test
  public void getDataDifference_same_data_case_return_zero() {
    final TableInformation source = new TableInformation().schemaName("test").tableName("client");
    final TableInformation destination =
        new TableInformation().schemaName("test").tableName("user");
    final List<String> columns = List.of("Id", "Firstname", "Lastname");
    assertThat(tableRepository.getDataDifference(destination, source, columns)).isEqualTo(0);
  }
}
