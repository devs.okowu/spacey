package eu.okowu.spacey.table.comparator;

import static org.assertj.core.api.Assertions.assertThat;

import eu.okowu.spacey.model.ObjectNameComparison;
import eu.okowu.spacey.model.TableInformation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ObjectNameComparatorTest {

  @Test
  public void compare_equal_object_names_return_null() {
    final TableInformation source = new TableInformation().schemaName("Mybank").tableName("card");
    final TableInformation destination =
        new TableInformation().schemaName("mybanK").tableName("CaRd");

    assertThat(ObjectNameComparator.compare(source, destination)).isNull();
  }

  @Test
  public void compare_different_object_names_return_object_name_comparison() {
    final TableInformation source = new TableInformation().schemaName("Mybank").tableName("card");
    final TableInformation destination =
        new TableInformation().schemaName("YourBank").tableName("CaRd");

    assertThat(ObjectNameComparator.compare(source, destination))
        .extracting(
            ObjectNameComparison::getSourceObjectName,
            ObjectNameComparison::getDestinationObjectName)
        .containsExactly("Mybank.card", "YourBank.CaRd");
  }
}
