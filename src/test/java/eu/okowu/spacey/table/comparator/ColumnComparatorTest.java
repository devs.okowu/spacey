package eu.okowu.spacey.table.comparator;

import static org.assertj.core.api.Assertions.assertThat;

import eu.okowu.spacey.model.ColumnComparison;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ColumnComparatorTest {

  @Test
  public void compare_on_two_identical_list_of_columns_return_column_comparison_object() {

    final List<String> columns = List.of("Id", "Serie", "Price", "Model", "Name");

    final List<String> sourceColumns = new ArrayList<>(columns);
    final List<String> destinationColumns = new ArrayList<>(columns);

    final ColumnComparison columnComparison =
        ColumnComparator.compare(sourceColumns, destinationColumns);

    assertThat(columnComparison)
        .extracting(
            ColumnComparison::getSourceCount,
            ColumnComparison::getDestinationCount,
            ColumnComparison::getOnlyInSource,
            ColumnComparison::getOnlyInDestination)
        .containsExactly(5, 5, Collections.emptyList(), Collections.emptyList());
    assertThat(columnComparison.getIdentical()).contains("Id", "Serie", "Price", "Model", "Name");
  }

  @Test
  public void compare_on_two_different_list_of_columns_return_column_comparison_object() {
    final List<String> sourceColumns = List.of("Id", "Serie", "Price", "Model", "Name");
    final List<String> destinationColumns =
        List.of("Id", "Series", "Price", "Model", "Name", "Year");

    final ColumnComparison columnComparison =
        ColumnComparator.compare(sourceColumns, destinationColumns);

    assertThat(columnComparison)
        .extracting(ColumnComparison::getSourceCount, ColumnComparison::getDestinationCount)
        .containsExactly(5, 6);
    assertThat(columnComparison.getIdentical()).contains("Id", "Model", "Price", "Name");
    assertThat(columnComparison.getOnlyInSource()).containsOnly("Serie");
    assertThat(columnComparison.getOnlyInDestination()).contains("Series", "Year");
  }
}
