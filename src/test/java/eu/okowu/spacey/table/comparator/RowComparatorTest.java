package eu.okowu.spacey.table.comparator;

import static org.assertj.core.api.Assertions.assertThat;

import eu.okowu.spacey.model.RowComparison;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class RowComparatorTest {

  @Test
  public void compare_equals_amount_of_rows_return_null() {
    final int sourceRows = 5;
    final int destinationRows = 5;

    assertThat(RowComparator.compare(sourceRows, destinationRows)).isNull();
  }

  @Test
  public void compare_different_amount_of_rows_return_row_comparison_object() {
    final int sourceRows = 5;
    final int destinationRows = 2;

    final RowComparison rowComparison = RowComparator.compare(sourceRows, destinationRows);

    assertThat(rowComparison)
        .extracting(RowComparison::getSourceRows, RowComparison::getDestinationRows)
        .containsExactly(sourceRows, destinationRows);
  }
}
